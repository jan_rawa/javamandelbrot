import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class TestMandelbrot {
    private int[] sizes;
    private int[] threads;
    /**
     * Takes an array each of diffrent values to test
     *
     */
    TestMandelbrot(int[] sizes, int[] threads) {
        this.sizes = sizes;
        this.threads = threads;
    }

    void testSizes() {
        System.out.println("size\ttime elapsed");
        for (int size : sizes) {
            Mandelbrot mandelbrot = new Mandelbrot(-.5, 0, 2.5, size, 255);

            Instant start = Instant.now();

            // repeat for mean
            for (int j = 0; j < 5; ++j)
                mandelbrot.znSection(0, mandelbrot.getN());

            Instant finish = Instant.now();
            long timeElapsed = Duration.between(start, finish).toMillis() / 5;

            System.out.println(size + "\t" + timeElapsed);
        }
    }

    void testThreads() {
        System.out.println("threads count\ttime elapsed\tsize = " + sizes[sizes.length / 2]);
        for (int threadCount : threads) {
            Mandelbrot mandelbrot = new Mandelbrot(-.5, 0, 2.5, sizes[sizes.length / 2], 255);

            Instant start = Instant.now();

            // repeat for mean
            for (int j = 0; j < 5; ++j) {

                Thread[] threads = new Thread[threadCount];
                for (int i = 0; i < threads.length; ++i) {
                    int indexStart = i*mandelbrot.getN()/threads.length;
                    int indexStop = (i+1)*mandelbrot.getN()/threads.length;

                    threads[i] = new Thread() {
                        public void run() {
                            mandelbrot.znSection(indexStart, indexStop);
                        }
                    };
                    threads[i].start();
                }

                for (var t : threads) {
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            Instant finish = Instant.now();
            long timeElapsed = Duration.between(start, finish).toMillis() / 5;

            System.out.println(threadCount + "\t" + timeElapsed);
        }
    }
}
