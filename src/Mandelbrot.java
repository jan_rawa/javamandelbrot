// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static java.lang.StrictMath.sin;

public class Mandelbrot {
    private double xc;
    private double yc;
    private double size;
    private int n;
    private int MAX_ITER;
    private static BufferedImage image;

    public Mandelbrot(double xc, double yc, double size, int n, int MAX_ITER) {
        this.xc = xc;
        this.yc = yc;
        this.size = size;
        this.n = n;
        this.MAX_ITER = MAX_ITER;
        image = new BufferedImage(n, n, TYPE_INT_RGB);
    }

    private static void run() {

    }

    int getN() {
        return n;
    }
    void save() throws IOException {
        File outputfile = new File("saved.png");
        ImageIO.write(image, "png", outputfile);
    }

    public void znSection(int startIndex, int endIndex) {
        for (int i = startIndex; i < endIndex; i++) {
            for (int j = 0; j < n; j++) {
                double x0 = xc - size/2 + size*i/n;
                double y0 = yc - size/2 + size*j/n;
                Complex z0 = new Complex(x0, y0);
                int escapeIterations = zn(z0, MAX_ITER);
                // set color varying hue based on escape iterations:
                Color color = Color.getHSBColor(
                        (float)escapeIterations / (float)MAX_ITER,
                        1.0f,
                        escapeIterations==MAX_ITER?0:1.0f
                );

                image.setRGB(i, n-1-j, color.getRGB());
            }
        }
    }
    private static int zn(Complex z0, int MAX_ITER) {
        Complex z = z0;
        for (int t = 0; t < MAX_ITER; t++) {
            if (z.abs() > 2.0) return t;
            z = z.times(z).plus(z0);
        }
        return MAX_ITER;
    }
}