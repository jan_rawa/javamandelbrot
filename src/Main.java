import java.io.IOException;

public class Main {

    public static void main(String[] args)  {


        int[] sizes = new int[16];
        int[] threadCounts = new int[16];

        for (int i = 0; i < sizes.length; ++i) {
            sizes[i] = 256*(i+1);
            threadCounts[i] = (i+1);
        }

        TestMandelbrot testMandelbrot = new TestMandelbrot(sizes, threadCounts);

        testMandelbrot.testSizes();
        testMandelbrot.testThreads();


        Mandelbrot mandelbrot = new Mandelbrot(-.5, 0, 2.5, 1080, 255);
        try {
            // retrieve image
            mandelbrot.save();
        } catch (IOException e) {
            System.out.println("Failed to print image");
        }
    }
}
